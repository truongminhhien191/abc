## Description
This application provides an HTTP API to store and retrieve files.
The application has two parts:
- A web interface helps the operation is easy and intuitive
- A RESTful API using by command line help to integrate to CICD or any scripts
#### Feature of application:
- Upload a new file (include to checking file name and content if duplicate)
- Get a uploaded files list
- Retrieve an uploaded file by name
- Delete an uploaded file by name
 
## Usage
Follow step by step in order to run application with Docker:
Step 1: Clone source code from repository

    git clone https://gitlab.com/truongminhhien191/filemanagement.git
    
  Step 2: Create .env file copy from .env.sample
  .env file contain information of application when start (eg. host, port) 
  
    cd filemanagement.git
    cp .env.sample .env
    
Step 3: Build docker images

    Docker build -t filemanagement:1.0 .
    
Step 4: Run application with Docker run

    docker run -d --name app --restart unless-stopped -p 5000:5000 -v $(pwd)/uploads:/app/uploads filemanagement:1.0
 
 Step 4: Go to http://localhost:5000 and view application started ( or http://<*public_ipaddress*>:5000)

## Testing

#### Using web interface
Go to http://localhost:5000, we have a web interface bellow

![](https://lh4.googleusercontent.com/We_DQgbnEaoSDbPd3liRDE7PpL9cpAGztxusmV0wJ1mWhn5DZlKYlAGH4JHcVpBrn4g02bSdzNw7D1tRh1zv=w3840-h2178-rw)

- Part 1: We can upload a new file
- Part 2: Show uploaded files list (about  Name, Date upload, Size)
- Part 3: Delete an uploaded file
- Part 4: View or download an uploaded file

#### using RESTful API
##### 1. Upload a new file

    curl -X POST http://localhost:5000/api/upload  -F "file=@/path/to/file"
    # Sample
    curl -X POST http://localhost:5000/api/upload  -F "file=@/tmp/test.txt"
    
Result:

    # True
	{
		"status": "Upload successfully !"
	}

	# False:
	if file name exist
	{
		"status": "File is exist !"
	}
	# Or if file upload has similar contents,
	{
		"status": "File is same content  with test.txt"
	}

##### 2. Get an uploaded files list

    curl -X GET http://localhost:5000/api/retrievelist
   Result:
   
    {
		"list": [
			[
				1,
				"text1.txt",
				"Mon Jan 14 01:40:45 2019",
				0.012
			],
			[
				2,
				"text2.txt",
				"Mon Jan 14 01:36:17 2019",
				12311.508
			]
		]
	}
##### 3.  Download an uploaded file

    curl -X GET http://localhost:5000/api/retrieve/<filename> --output <filename>
    # Sample
    curl -X GET http://localhost:5000/api/retrieve/test.txt --output test.txt

Result

    True: file was downloaded
    False:
	{
		"status": "File not Found !"
	}

##### 4. Delete an uploaded file

    curl -X POST http://localhost:5000/api/delete/<filename>
    # Sample
    curl -X POST http://localhost:5000/api/delete/test.txt
   Result
   

    True:
	{
		"status": "Delete successfully !"
	}

	False:
	{
		"status": "File not found !"
	}
	

